/**
 * Too Pooped to Pop.
 * random walk with random angle makes telephone receiver curl code.
 * 
 * @author @deconbatch
 * @version 0.1
 * Processing 3.2.1
 * 2019.05.26
 */

void setup() {
  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  blendMode(DIFFERENCE);
  smooth();
  noStroke();
  noLoop();
}

void draw() {

  int   frmCntMax  = 24 * 6;  // for 24fps x 6s animation
  int   lineCntMax = 9;
  float hueInit    = random(360.0);
  
  translate(width * 0.5, height * 0.5);
  for (int frmCnt = 0; frmCnt < frmCntMax; ++frmCnt) {

    float frmRatio = map(frmCnt, 0, frmCntMax, 0.0, 1.0);
    float hueBase  = hueInit;

    background(0.0, 0.0, 90.0, 100.0);
    for (int lineCnt = 0; lineCnt < lineCntMax; ++lineCnt) {
      float lineRatio = map(lineCnt, 0, lineCntMax, 0.0, 1.0);
      float easeRatio = easeInOutQuadratic((frmRatio + lineRatio) % 1.0);
      pushMatrix();
      rotate(TWO_PI * lineRatio);
      fill(hueBase % 360.0, 80, 10.0, 100.0);
      drawLine(lineRatio, easeRatio);
      hueBase += 30.0;
      popMatrix();
    }

    saveFrame("frames/" + String.format("%04d", frmCnt) + ".png");

  }

  exit();

}

/**
 * drawLine
 * @param  _phase    0.0 - 1.0 : make line shape with this.
 * @param  _animate  0.0 - 1.0 : animate line shape with this.
 */
private void drawLine(float _phase, float _animate) {

  float rotation = 0.0;

  for (float radius = 0; radius <= width; radius += 1.0) {

    float rRatio = map(radius, 0, width, 0.0, 1.0);

    // make line curve with noise
    float pointX = 0.7 * radius * cos(TWO_PI * noise(rRatio * 0.1));
    float pointY = 0.7 * radius * sin(TWO_PI * noise(rRatio * 0.1));

    // line shape and animation
    float lShape   = sin(TWO_PI * (_phase + rRatio)) * 0.1;
    float lAnimate = sin(PI * _animate) * 0.05;  // cyclic and slow
    rotation += lShape + lAnimate;

    float eSiz    = sin(PI * rRatio) * 3.0 + 2.0;
    float eRadius = 2.0 + eSiz + rRatio * 30.0;  // IMPORTANT! make line curl
    
    pushMatrix();
    translate(pointX, pointY);
    rotate(rotation);
    ellipse(eRadius, eRadius, eSiz * eSiz, eSiz * eSiz);
    popMatrix();
      
  }
}

/**
 * easeInOutQuadratic easing function.
 * @param  _t    0.0 - 1.0 : linear value.
 * @return float 0.0 - 1.0 : eased value.
 */
private float easeInOutQuadratic(float _t) {
  _t *= 2.0;
  if (_t < 1.0) {
    return pow(_t, 2) / 2.0;
  }
  _t -= 1.0;
  return -(_t * (_t - 2) - 1.0) / 2.0;
}
